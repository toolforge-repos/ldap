// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2017, 2023 Kunal Mehta <legoktm@debian.org>

use anyhow::Result;
use ldap3::{dn_escape, Ldap, LdapError, ResultEntry, Scope, SearchEntry};
use serde::Serialize;

pub(crate) trait MemberInfoLookup {
    const ATTR_LIST: [&'static str; 2];

    fn from_search_entry(searched: SearchEntry) -> Self;
}

#[derive(Serialize, Debug)]
pub(crate) struct MemberInfo {
    cn: String,
    uid: String,
    mail: String,
    uid_number: String,
    sul: Option<String>,
    ssh_keys: Vec<String>,
    created: String,
    groups: Vec<String>,
    servicegroups: Vec<String>,
    disabled: bool,
}

impl MemberInfoLookup for MemberInfo {
    const ATTR_LIST: [&'static str; 2] = ["+", "*"];

    fn from_search_entry(mut searched: SearchEntry) -> Self {
        let mut groups = vec![];
        let mut servicegroups = vec![];
        if let Some(member_of) = searched.attrs.get("memberOf") {
            for group in member_of {
                match parse_group(group) {
                    (GroupType::Group, name) => {
                        groups.push(name);
                    }
                    (GroupType::ServiceGroup, name) => {
                        servicegroups.push(name);
                    }
                }
            }
            groups.sort();
            servicegroups.sort();
        }

        Self {
            cn: searched
                .attrs
                .remove("cn")
                .map(|mut v| v.remove(0))
                .unwrap_or_else(|| "unknown".to_string()),
            uid: searched
                .attrs
                .remove("uid")
                .map(|mut v| v.remove(0))
                .unwrap_or_else(|| "unknown".to_string()),
            mail: obfuscate_email(
                &searched
                    .attrs
                    .remove("mail")
                    .map(|mut v| v.remove(0))
                    .unwrap_or_else(|| "unknown".to_string()),
            ),
            uid_number: searched
                .attrs
                .remove("uidNumber")
                .map(|mut v| v.remove(0))
                .unwrap_or_else(|| "unknown".to_string()),
            ssh_keys: searched
                .attrs
                .remove("sshPublicKey")
                .unwrap_or_default()
                .iter()
                .map(|k| obfuscate_email(k))
                .collect(),
            sul: searched
                .attrs
                .remove("wikimediaGlobalAccountName")
                .map(|mut v| v.remove(0)),
            // FIXME: pretty formatting
            created: searched
                .attrs
                .remove("createTimestamp")
                .map(|mut v| v.remove(0))
                .unwrap_or_else(|| "unknown".to_string()),
            groups,
            servicegroups,
            disabled: searched.attrs.get("pwdPolicySubentry")
                == Some(&vec![
                    "cn=disabled,ou=ppolicies,dc=wikimedia,dc=org".to_string()
                ]),
        }
    }
}

#[derive(Serialize, Debug)]
pub(crate) struct LightMemberInfo {
    cn: String,
    uid: String,
}

impl MemberInfoLookup for LightMemberInfo {
    const ATTR_LIST: [&'static str; 2] = ["cn", "uid"];

    fn from_search_entry(mut searched: SearchEntry) -> Self {
        Self {
            cn: searched
                .attrs
                .remove("cn")
                .map(|mut v| v.remove(0))
                .unwrap_or_else(|| "unknown".to_string()),
            uid: searched
                .attrs
                .remove("uid")
                .map(|mut v| v.remove(0))
                .unwrap_or_else(|| "unknown".to_string()),
        }
    }
}

/// Do a little obfuscation of the email
fn obfuscate_email(input: &str) -> String {
    input.replace('@', " at ").replace('.', " dot ")
}

/// Given a cn, find the uid if it exists
pub(crate) async fn cn_to_uid(ldap: &mut Ldap, cn: &str) -> Result<Option<String>> {
    let result = ldap_query(
        ldap,
        "dc=wikimedia,dc=org",
        Scope::Subtree,
        &format!("cn={}", dn_escape(cn)),
        vec!["uid"],
    )
    .await?;

    match result {
        Some(result) => match result.into_iter().next() {
            Some(entry) => {
                let searched = SearchEntry::construct(entry);
                Ok(searched.attrs.get("uid").map(|v| v[0].clone()))
            }
            None => Ok(None),
        },
        None => Ok(None),
    }
}

async fn ldap_query(
    ldap: &mut Ldap,
    base: &str,
    scope: Scope,
    filter: &str,
    attrs: Vec<&str>,
) -> Result<Option<Vec<ResultEntry>>> {
    let result = ldap.search(base, scope, filter, attrs).await?.success();
    match result {
        Ok((rs, _)) => Ok(Some(rs)),
        Err(err) => {
            if let LdapError::LdapResult { result: res } = &err {
                // 32 = noSuchObject
                if res.rc == 32 {
                    return Ok(None);
                }
            }
            Err(err.into())
        }
    }
}

#[derive(Serialize, Debug)]
pub(crate) struct GroupInfo {
    group: String,
    members: Vec<LightMemberInfo>,
    groups: Vec<String>,
    gid: Option<usize>,
    last_modified: Option<String>,
}

pub(crate) async fn group_info(ldap: &mut Ldap, group: &str) -> Result<Option<GroupInfo>> {
    let results = ldap_query(
        ldap,
        "dc=wikimedia,dc=org",
        Scope::Subtree,
        &format!(
            "(&(|(objectclass=groupOfNames)(objectclass=groupofnames))(cn={}))",
            dn_escape(group)
        ),
        vec!["+", "*"],
    )
    .await?;
    let result = match results {
        Some(results) => results,
        None => {
            return Ok(None);
        }
    };
    if let Some(entry) = result.into_iter().next() {
        let mut searched = SearchEntry::construct(entry);
        let mut members = vec![];
        let mut groups = vec![];
        if let Some(member_attr) = searched.attrs.get("member") {
            for member in member_attr {
                match parse_groupmember(member) {
                    Some(GroupMember::Person(member_dn)) => {
                        let info: Option<LightMemberInfo> = member_info(ldap, &member_dn).await?;
                        members.push(info.unwrap_or_else(|| LightMemberInfo {
                            cn: "<unknown user>".to_string(),
                            uid: member_dn,
                        }));
                    }
                    Some(GroupMember::Group(group)) => {
                        groups.push(group);
                    }
                    None => {}
                }
            }
        }
        let gid: Option<usize> = searched
            .attrs
            .get("gidNumber")
            .and_then(|val| val[0].parse().ok());
        let last_modified = searched
            .attrs
            .remove("modifyTimestamp")
            .map(|mut v| v.remove(0));
        members.sort_by_cached_key(|m| m.cn.to_string());
        Ok(Some(GroupInfo {
            group: group.to_string(),
            members,
            groups,
            gid,
            last_modified,
        }))
    } else {
        Ok(None)
    }
}

pub(crate) async fn member_info<T: MemberInfoLookup>(
    ldap: &mut Ldap,
    username: &str,
) -> Result<Option<T>> {
    let results = ldap_query(
        ldap,
        &format!("uid={},ou=people,dc=wikimedia,dc=org", dn_escape(username)),
        Scope::Base,
        "(objectclass=*)",
        T::ATTR_LIST.to_vec(),
    )
    .await?;
    let result = match results {
        Some(results) => results,
        None => {
            return Ok(None);
        }
    };
    if let Some(entry) = result.into_iter().next() {
        let searched = SearchEntry::construct(entry);
        let info = T::from_search_entry(searched);
        return Ok(Some(info));
    }
    Ok(None)
}

enum GroupType {
    Group,
    ServiceGroup,
}

fn parse_group(input: &str) -> (GroupType, String) {
    let (_, name) = input.split_once('=').unwrap();
    let (name, _) = name.split_once(',').unwrap();
    let group = if input.contains("ou=servicegroups") {
        GroupType::ServiceGroup
    } else {
        GroupType::Group
    };
    (group, name.to_string())
}

enum GroupMember {
    Person(String),
    Group(String),
}

fn parse_groupmember(input: &str) -> Option<GroupMember> {
    let sp: Vec<_> = input.split(',').collect();
    if sp.get(1) == Some(&"ou=people") {
        let uid = sp[0].strip_prefix("uid=").map(|x| x.to_string())?;
        // Toolforge tools can have groups be members of groups
        if input.contains("ou=servicegroups") {
            Some(GroupMember::Group(uid))
        } else {
            Some(GroupMember::Person(uid))
        }
    } else {
        None
    }
}
