// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2017, 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use crate::lookup::{GroupInfo, MemberInfo};
use cached::proc_macro::cached;
use ldap3::{Ldap, LdapConnAsync};
use mwbot::parsoid::WikinodeIterator;
use mwbot::Bot;
use rocket::{response::Redirect, serde::json::Json};
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::{collections::BTreeSet, path::Path};

mod lookup;

// TODO: don't hardcode, get from /etc/ldap.conf
const LDAP_SERVER: &str = "ldaps://ldap-ro.eqiad.wikimedia.org:636";
// for local debugging with:
// $ ssh -N dev.toolforge.org -L 3389:ldap-ro.eqiad.wikimedia.org:389
const LOCAL_LDAP_SERVER: &str = "ldap://localhost:3389";

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(status = 500, content_type = "text/html")]
struct Error(Template);

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Self(Template::render(
            "error",
            ErrorTemplate {
                code: 500,
                reason: "Internal Server Error",
                error: Some(value.to_string()),
            },
        ))
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    groups: BTreeSet<String>,
}

async fn conn() -> anyhow::Result<Ldap> {
    let server = if Path::new("/etc/wmcs-project").exists() {
        LDAP_SERVER
    } else {
        LOCAL_LDAP_SERVER
    };
    let (conn, ldap) = LdapConnAsync::new(server).await?;
    ldap3::drive!(conn);
    Ok(ldap)
}

/// Fetch the groups out of the wiki page, cache for an hour
#[cached(sync_writes = true, result = true, time = 3600)]
async fn fetch_groups() -> anyhow::Result<BTreeSet<String>> {
    let bot = Bot::builder("https://wikitech.wikimedia.org/w/".to_string())
        .build()
        .await?;
    let page = bot.page("SRE/LDAP/Groups")?;
    let html = page.html().await?.into_mutable();
    let mut groups = BTreeSet::new();
    for element in html.select("code.group") {
        groups.insert(element.text_contents());
    }
    Ok(groups)
}

#[get("/")]
async fn index() -> Result<Template> {
    let groups = fetch_groups().await?;
    Ok(Template::render("index", IndexTemplate { groups }))
}

#[get("/api.json")]
async fn index_api() -> Result<Json<IndexTemplate>> {
    let groups = fetch_groups().await?;
    Ok(Json(IndexTemplate { groups }))
}

#[get("/user/<username>")]
async fn user(username: &str) -> Result<Option<Template>> {
    let mut ldap = conn().await?;
    let info: Option<MemberInfo> = lookup::member_info(&mut ldap, username).await?;

    Ok(info.map(|info| Template::render("user", info)))
}

#[get("/cn/<username>")]
async fn cn(username: &str) -> Result<Option<Redirect>> {
    let mut ldap = conn().await?;
    let uid = lookup::cn_to_uid(&mut ldap, username).await?;
    if let Some(uid) = uid {
        Ok(Some(Redirect::to(format!("/user/{uid}"))))
    } else {
        Ok(None)
    }
}

#[get("/api/user/<username>")]
async fn user_api(username: &str) -> Result<Option<Json<MemberInfo>>> {
    let mut ldap = conn().await?;
    let info = lookup::member_info(&mut ldap, username).await?;
    Ok(info.map(Json))
}

#[get("/api/cn/<username>")]
async fn cn_api(username: &str) -> Result<Option<Redirect>> {
    let mut ldap = conn().await?;
    let uid = lookup::cn_to_uid(&mut ldap, username).await?;
    if let Some(uid) = uid {
        Ok(Some(Redirect::to(format!("/api/user/{uid}"))))
    } else {
        Ok(None)
    }
}

#[get("/group/<group>")]
async fn group(group: &str) -> Result<Option<Template>> {
    let mut ldap = conn().await?;
    let info = lookup::group_info(&mut ldap, group).await?;
    Ok(info.map(|info| Template::render("group", info)))
}

#[get("/api/group/<group>")]
async fn group_api(group: &str) -> Result<Option<Json<GroupInfo>>> {
    let mut ldap = conn().await?;
    let info = lookup::group_info(&mut ldap, group).await?;
    Ok(info.map(Json))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount(
            "/",
            routes![index, index_api, group, group_api, user, user_api, cn, cn_api],
        )
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}

#[cfg(test)]
mod tests {
    use rocket::http::Status;
    use rocket::local::asynchronous::Client;

    use super::*;

    #[tokio::test]
    async fn test_fetch_groups() {
        let groups = fetch_groups().await.unwrap();
        assert!(groups.contains("nda"));
        assert!(groups.contains("ops"));
        assert!(groups.contains("wmf"));
    }

    #[tokio::test]
    async fn test_routes() {
        let client = Client::tracked(rocket()).await.unwrap();
        let index = client.get("/").dispatch().await;
        assert_eq!(index.status(), Status::Ok);
    }

    /// Test routes that need a connection to the LDAP server
    #[tokio::test]
    async fn test_routes_ldap() {
        if conn().await.is_err() {
            // No LDAP connection, just skip
            println!("Skipping LDAP routes because no server connection");
            return;
        }
        let client = Client::tracked(rocket()).await.unwrap();
        // basic HTTP 200
        let ops_group = client.get("/group/ops").dispatch().await;
        assert_eq!(ops_group.status(), Status::Ok);
        let user_bd808 = client.get("/user/bd808").dispatch().await;
        assert_eq!(user_bd808.status(), Status::Ok);
        // test the `/cn/` redirector
        let cn_bd808 = client.get("/cn/BryanDavis").dispatch().await;
        assert_eq!(cn_bd808.status(), Status::SeeOther);
        assert_eq!(
            cn_bd808.headers().get_one("location").unwrap(),
            "/user/bd808"
        );
        let cn_matmarex = client.get("/cn/Bartosz%20Dziewo%C5%84ski").dispatch().await;
        assert_eq!(cn_matmarex.status(), Status::SeeOther);
        assert_eq!(
            cn_matmarex.headers().get_one("location").unwrap(),
            "/user/matmarex"
        );
        // basic HTTP 404
        assert_eq!(
            client
                .get("/group/non-existent-group")
                .dispatch()
                .await
                .status(),
            Status::NotFound
        );
        assert_eq!(
            client
                .get("/user/non-existent-user")
                .dispatch()
                .await
                .status(),
            Status::NotFound
        );
        // disabled account
        let disabled = client.get("/user/evuvave").dispatch().await;
        assert!(disabled
            .into_string()
            .await
            .unwrap()
            .contains("Account disabled"));
    }
}
